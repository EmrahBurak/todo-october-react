import AddTodo from "./components/AddTodo";
import Control from "./components/Control";
import Nav from "./components/Nav";

function App() {
  return (
    <div className="container my-5">
      <div className="row">
        <div className="col-5 mx-auto">
          <Nav name={"Emrah"}/>
          <AddTodo/>
          <Control/>
        </div>
      </div>

    </div>

  );
}

export default App;
