import React,{useState,useEffect} from 'react'
import {useDispatch} from 'react-redux'
import {addTodo} from '../../redux/Todo/todo.action'

const initial_state = {description:"",action:false}

const AddTodo = ()=>{
    const [inputText,setInputText] = useState(initial_state);
    const dispatch = useDispatch();

    useEffect(()=> {
        console.log("AddTodo -- Rendering");

    },[])

    
    const handleOnClick = e => {
        e.preventDefault();
        // console.log("Cilcked: " ,inputText);
        dispatch(addTodo(inputText));
        setInputText(initial_state);

        // action
    }

    const handleOnChange = e => {
        setInputText( prev => ({...inputText,[e.target.name]:e.target.value}));

    }

    return(
        <div className="input-group mt-3">
            <input placeholder="Create some todo.." type="text"
             name="description"  className="form-control"
             onChange={handleOnChange}
             value={inputText.description}
             />
            <button type="submit" onClick={handleOnClick}   className="btn btn-primary">Add</button>
        </div>
    );
}

export default AddTodo;