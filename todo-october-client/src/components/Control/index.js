import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import TodoTable from "../TodoTable";
import { setDisplayAction } from "../../redux/Todo/todo.action";

const Control = () => {
  const [display, setDisplay] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log(display);
    dispatch(setDisplayAction(display));
  }, [display]);

  return (
    <div className="form-check my-4">
      <label>Display All</label>
      <input
        type="checkbox"
        checked={display}
        onChange={() => setDisplay((prev) => !display)}
        className="form-check-input"
      />
      <TodoTable />
    </div>
  );
};

export default Control;
