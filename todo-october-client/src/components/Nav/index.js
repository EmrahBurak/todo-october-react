import React from "react";

const Nav = ({name}) => {
  return (
    <div>
      <nav className="navbar navbar-light text-white bg-success bg-gradient ">
          <h3 className="text-white mx-auto">{name}'s Todo List </h3>
      </nav>
    </div>
  );
};

export default Nav;