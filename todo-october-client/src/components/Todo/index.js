import React,{Fragment} from 'react'
import {updateTodo,deleteTodo} from '../../redux/Todo/todo.action'
import {useDispatch} from 'react-redux'


const Todo = ({display, todo,index,setTodoAction }) => {
  const dispatch = useDispatch();

  const handleOnChange = (e) => {
    setTodoAction(!todo.action);
    dispatch(updateTodo({...todo,[e.target.name]:!todo.action}));
  }

  const handleClick = e => {
    console.log("deleted");
    dispatch(deleteTodo(todo));

  }

  return (
    <>
        <td>{index + 1}</td>
        <td>{todo.description}</td>
        <td>{todo.action}</td>
        <td>
          <input type="checkbox" name="action" 
          onChange={handleOnChange} checked={todo.action} />
        </td>
        {
          display && (
            <td>
              <button className="btn btn-danger btn-sm" onClick={handleClick}> Delete</button>
            </td>
          )
        }
        </>
  );
};

export default Todo;
