import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Todo from "../Todo";
import { fetchTodos } from "../../redux/Todo/todo.action";

const mapState = (state) => ({
  todosData: state.todosData,
});

const TodoTable = () => {
  const [fromTodoAction, setFromTodoAction] = useState(null);
  const { todosData } = useSelector(mapState);
  const { todos, display } = todosData;
  const len = todos.length;
  const dispatch = useDispatch();

  const handleTodoAction = (checkState) => {
    setFromTodoAction(checkState);
  };

  useEffect(() => {
    console.log("TodoTable first rendering..");
    dispatch(fetchTodos());
    setFromTodoAction(null);
  }, [display, len, fromTodoAction]);

  return (
    <div>
      {len === 0 ? (
        <div className="alert alert-info mt-3">
          Finished! Lets create some todo..
        </div>
      ) : (
        <table className="table">
          <thead>
            <tr>
              <td>id</td>
              <td>description</td>
              <td>action</td>
            </tr>
          </thead>
          <tbody>
            {todosData.todos.map((todo, pos) => (
              <tr
                key={pos}
                className={`alert ${
                  todo.action ? "alert-success" : "alert-warning"
                }`}
              >
                <Todo
                  display={display}
                  todo={todo}
                  index={pos}
                  setTodoAction={handleTodoAction}
                />
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default TodoTable;
