import { todoTypes } from "./todo.types";
const axios = require('axios');
const uri = "http://localhost:3001/todos";



export const fetchTodoRequest = (todos) => ({
    type: todoTypes.FETCH_TODO_REQUEST,
    payload: todos
});


export const addTodoAction = (todo) => ({
    type: todoTypes.ADD_TODO,
    payload: todo
});

export const updateTodoAction = (todo) => ({
    type: todoTypes.UPDATE_TODO,
    payload: todo
});

export const deleteTodoAction = (todo) => ({
    type: todoTypes.DELETE_TODO,
    payload: todo
})

export const setDisplayAction = display => ({
    type:todoTypes.SET_DISPLAY,
    payload: display
});



export const fetchTodos = () => async dispatch => {
    await axios.get(uri)
    .then(response => {
        let todos = response.data;
        dispatch(fetchTodoRequest(todos));
    })
    .catch(err => console.log(err.message));
}

export const addTodo = (todo) => async dispatch => {
    await axios.post(uri,todo)
    .then(response => {
        dispatch(addTodoAction(todo))
    })
    .catch(err => console.log(err.message));
}

export const updateTodo = todo => async dispatch => {
    await axios.put(uri+`/${todo.id}`,{
        id:todo.id,
        description:todo.description,
        action:todo.action
    })
    .then(response => {
        dispatch(updateTodoAction(todo))
    })
    .catch(err => console.log(err.message));
}


export const deleteTodo = todo => async dispatch => {
    await axios.delete(uri+`/${todo.id}`,todo)
    .then(response => {
        dispatch(deleteTodoAction(todo));
    })
    .catch(err => console.log(err.message));
}
