import { todoTypes } from "./todo.types";
import { handleUpdateTodo,handleAddTodo,
  handlefilterTodos, handleDeleteTodo } from "./todo.utils";

const INITIAL_STATE = {
  display:false,
  todos: [],
};



export const todoReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case todoTypes.FETCH_TODO_REQUEST:
      return {
        ...state,
        // todos:[...action.payload].filter(x => x.action === state.display)
        todos: handlefilterTodos({
          todos:action.payload,
          display:state.display
        }),
      };
    case todoTypes.ADD_TODO:
      return {
        ...state,
        todos: handleAddTodo({
            prevTodos:state.todos,
            newTodo:action.payload
        })
      };
    case todoTypes.UPDATE_TODO:
      return {
        ...state,
        todos: handleUpdateTodo({
          prevTodos: state.todos,
          newTodo: action.payload,
        }),
      };
    case todoTypes.DELETE_TODO:
      return{
        ...state,
        todos:handleDeleteTodo({
          prevTodos:state.todos,
          removeTodo:action.payload
        }),
      }
    case todoTypes.SET_DISPLAY:
      return{
        ...state,
        display:action.payload
      }
     default:
      return state;
  }
};
