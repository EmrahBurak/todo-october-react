require('dotenv').config();

const existingTodo = ( prevTodos, newTodo ) => {
  return prevTodos.find((todo) => todo.id === newTodo.id) ?? null;
};

export const handlefilterTodos = ({ todos, display }) => {
  if(display === false){
    return todos.filter((x) => x.action === display);
  }
  return [...todos];
};

export const handleUpdateTodo = ({ prevTodos, newTodo }) => {
     return prevTodos.reduce((acc, item) => {
      if (item.id === newTodo.id) {
        item = newTodo;
      }
      return [...acc, item];
    }, []);
};

export const handleAddTodo = ({ prevTodos, newTodo }) => {
  const todoExist = existingTodo(prevTodos,newTodo);
  if (todoExist == null) {
    return [...prevTodos, newTodo];
  }
};

export const handleDeleteTodo = ({prevTodos,removeTodo}) => {
  const todoExist = existingTodo(prevTodos,removeTodo);
  return todoExist ? prevTodos.filter(x => x.id !== removeTodo.id) : [...prevTodos];

}

